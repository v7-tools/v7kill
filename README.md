# v7kill




## Description
v7kill is the kill program (`/bin/kill`) found in UNIX V7, updated for modern compilers.

## Notes while installing
If you get `cc: command not found`, even though you have installed a C compiler, such as GCC, you may have to append the `CC=` argument. 

If you are using GCC use this: `make CC=gcc`

If you are using Clang use this: `make CC=clang`

## License
v7kill is licensed under the 3-clause BSD License.
